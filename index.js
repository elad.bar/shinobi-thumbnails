const { Worker } = require('worker_threads');
const fs = require('fs');
const path = require('path');

module.exports = (s, shinobiConfig, lang, app, io) => {
    const { splitForFFPMEG } = require('../../ffmpeg/utils.js')(s, shinobiConfig, lang);
    const { processKill } = require('../../monitor/utils.js')(s, shinobiConfig, lang);
    const snapProcessTimeout = {};
    
    const thumbConfig = {
        daysToKeep: null,
        dateToKeep: null
    };
    
    const initialize = () => {
        const superUsers = require("/config/super.json");
        const superUser = superUsers[0];

        const params = {
            username: superUser.mail,
            password: superUser.pass,
        };

        s.auth(params, function(user){
            thumbConfig.daysToKeep = user.details.days;

            createThumbnailDirectory();

            s.insertCompletedVideoExtensions.push(insertCompletedVideo);
        });
    };

    const setDateToKeep = () => {
        const daysAgo = new Date();
      
        daysAgo.setDate(daysAgo.getDate() - thumbConfig.daysToKeep);
      
        thumbConfig.dateToKeep = daysAgo.getTime();
    };

    const convertPathVideoToThumbnail = (videoPath) => {
        const result = videoPath.replace("videos", "thumbnails");

        return result;
    }

    const createThumbnailDirectory = () => {
        const thumbnailsDir = convertPathVideoToThumbnail(s.dir.videos)
        
        if(!fs.existsSync(thumbnailsDir)) {
            fs.mkdirSync(thumbnailsDir);
        }
    };

    const getThumbnailPath = (videoPath) => {
        const thumbnailsDir = convertPathVideoToThumbnail(videoPath);
        const thumbnailPath = `${thumbnailsDir}.jpg`;

        return thumbnailPath;
    };

    const shouldKeep = (filePath) => {
        const fileStats = fs.statSync(filePath);
        const result = thumbConfig.dateToKeep === null || fileStats.birthtimeMs > thumbConfig.dateToKeep;

        return result;
    }

    const cleanOldThumbnails = (directoryPath) => {
        const thumbnailsToDelete = fs.readdirSync(directoryPath)
                                     .map(file => {
                                         return path.join(directoryPath, file);
                                     })
                                     .filter(file => {
                                         return !shouldKeep(file);
                                     });

        thumbnailsToDelete.forEach(file => {
            fs.unlink(file, function () {});
        });
        
        const deletedThumbnails = thumbnailsToDelete.length;

        if (deletedThumbnails > 0) {
            console.info(`Deleted ${deletedThumbnails} thumbnails from ${directoryPath}`);
        }
    };

    const extractDirectoryFromFile = (filePath, removeParts) => {
        const filePathParts = filePath.split("/");
        const directoryPathParts = filePathParts.slice(0, -1 * removeParts);
        const directoryPath = directoryPathParts.join("/");

        return directoryPath
    };

    const createThumbnail = (videoPath) => {
        try {
            const thumbnailPath = getThumbnailPath(videoPath);
            
            if (!fs.existsSync(thumbnailPath)) {
                const libPath = extractDirectoryFromFile(__dirname, 2);
                const ffmpegCmd = splitForFFPMEG(`-y -ss 00:00:01 -i "${videoPath}" -frames:v 1 -q:v 2 -vf scale=256:-1 "${thumbnailPath}"`);

                const snapProcess = new Worker(`${libPath}/cameraThread/snapshot.js`, {
                    workerData: {
                        jsonData: {
                            cmd: ffmpegCmd,
                            temporaryImageFile: thumbnailPath,
                            useIcon: false
                        },
                        ffmpegAbsolutePath: shinobiConfig.ffmpegDir,
                    }
                });

                snapProcess.on('message', function (data) {
                    s.debugLog(data);
                });

                snapProcess.on('error', (data) => {
                    console.error(`Error while trying create thumbnail for${videoPath}, Data: ${data}`);;
                    
                    processKill(snapProcess);
                });

                snapProcess.on('exit', (code) => {
                    clearTimeout(snapProcessTimeout[videoPath]);
                });

                snapProcessTimeout[videoPath] = setTimeout(function () {
                    processKill(snapProcess);
                }, 2000);    
            }

        } catch (err) {
            console.error(`Failed to create thumbnail for${videoPath}, Error: ${err}`);
        } 
    };

    const ensureThumbnailsDirectory = (thumbnailsDir) => {
        if (!fs.existsSync(thumbnailsDir)) {
            fs.mkdirSync(thumbnailsDir,  { recursive: true});
        }
    };

    const insertCompletedVideo = (monitorConfig, videoDetails) => {
        const videoDirectory = videoDetails["dir"];
        const fileName = videoDetails["filename"];
        const videoPath = `${videoDirectory}${fileName}`;

        const thumbnailPath = getThumbnailPath(videoPath);
        const thumbnailsDir = extractDirectoryFromFile(thumbnailPath, 1);

        ensureThumbnailsDirectory(thumbnailsDir);

        createThumbnail(videoPath);

        setDateToKeep();
        cleanOldThumbnails(thumbnailsDir);
    }; 

    const setResponse = (res, filePath) => {
        fs.readFile(filePath, function (err, buffer) {
            if (err) {
                res.status(404).end();

            } else {
                res.send(buffer);
            };
        });
    };

    app.get(`${shinobiConfig.webPaths.apiPrefix}:auth/thumbnails/:ke`, function (req,res){
        s.auth(req.params,function(user){
            res.send({
                ok: true,
                message: "Module is up and running, waiting to serve"
            });
        },res,req);
    });

    app.get(`${shinobiConfig.webPaths.apiPrefix}:auth/thumbnails/:ke/:id/:time/:ext`, function (req,res){
        s.auth(req.params,function(user){
            const groupKey = req.params.ke;
            const monitorId = req.params.id;
            const videoTime = req.params.time;
            const videoExt = req.params.ext;

            res.contentType('image/jpeg');
            
            const monitorRestrictions = s.getMonitorRestrictions(user.details,monitorId)
            if(user.details.sub && user.details.allmonitors === '0' && (user.permissions.watch_videos === "0" || monitorRestrictions.length === 0)){
                res.status(403).end();
                
                return;
            }

            const url = `${s.dir.videos}${groupKey}/${monitorId}/${videoTime}.${videoExt}`;
            const thumbnailPath = getThumbnailPath(url);

            if (fs.existsSync(thumbnailPath)) {
                setResponse(res, thumbnailPath);

            } else {
                res.status(404).end();
            }           
            
        },res,req);
    });

    initialize();
};