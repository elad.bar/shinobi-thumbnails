# Shinobi Thumbnails

Shinobi Video NVR does not provide an endpoint for thumbnails, using this customAutoLoad module:

- Thumbnails will be created per monitor
- Thumbnails will get deleted once video files deleted
- Endpoint will provide any consumer ability to get thumbnail for specific file of specific monitor

## How to install

Get latest release from GitLab:

- Open `shinobi-thumbnails` [releases](https://gitlab.com/elad.bar/shinobi-thumbnails/-/releases)
- Copy the link of `Source code (zip)` asset of latest version

Install Shinobi customAutoLoad:

- Login to the super dashboard
- Open tab `Custom Auto Load`
- Paste the release asset ZIP file url in the `Download URL for Module`
- Click on `Download`
- In the package added to the list, click on `Enable`
- Restart Shinobi Video

*If previous version of the module is installed, remove it before updateing*

## How to use

### Endpoints

#### Verify Module Loaded

```bash
curl http://url/:auth/thumbnails/:ke
```

| HTTP Code  | Description                | Content              |
|------------|--------------------------- | -------------------- |
| 200        | Thumbnail is available     | JSON Below           |

```json
{
    "ok": true,
    "message": "Module is up and running, waiting to serve"
}
```

#### Get thumbnail

```bash
curl http://url/:auth/thumbnails/:ke/:id/:time/:ext
```

| HTTP Code  | Description                | Content              |
|------------|--------------------------- | -------------------- |
| 200        | Thumbnail is available     | Thumbnail image      |
| 403        | Unauthorized user          |                      |
| 404        | Thumbnail is not available |                      |
